
const FIRST_NAME = "";
const LAST_NAME = "";
const GRUPA = "";

/**
 * Make the implementation here
 */
function initCaching() {
    var cache = {};
    var obj = {};
    cache.pageAccessCounter = function (parameter) {   
        if (parameter == null && obj.hasOwnProperty('home'))
            obj['home'] = obj['home'] + 1; 
        else if (parameter == null && obj.hasOwnProperty('home') == false)
            obj['home'] = 1; 
        else {
            var pageName = String(parameter).toLowerCase();
            if (obj.hasOwnProperty(pageName))
                obj[pageName] = obj[pageName] + 1;
            else
                obj[pageName] = 1;
        }
    };
    cache.getCache = function () {
        return obj;
    };
    return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

